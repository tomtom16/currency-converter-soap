const soap = require('soap');

const assert = require('assert');
const RequestECBService = require('../services/RequestECBService');

// @todo Before you run these tests, you have to manually start app.js
describe('SoapClientTest', () => {
    let requestECBService = new RequestECBService();
    describe('basic', () => {
        it('should send and receive correctly correctly', (done) => {
            soap.createClient('wsdl/service-definition.wsdl' , (err, client) => {
                if (err) {
                    console.error(err);
                    return;
                }

                let args = {
                    fromCurrency: 'USD',
                    toCurrency: 'EUR',
                    fromCurrencyAmount: '100',
                };

                //client.setSecurity(new soap.BasicAuthSecurity('super-secure-use', 'secure-soap-password'));

                let options = {
                  passwordType: 'PasswordDigest',
                  hasNonce: true,
                  hasTokenCreated: true
                  //actor: 'actor'
                };
                let wsSecurity = new soap.WSSecurity('super-secure-user', 'secure-soap-password', options)
                client.setSecurity(wsSecurity);

                client.ExchangePortService.ExchangePortSoap11.exchangeCurrency(args, (err, result) => {
                    console.log('after close');
                    if (err) {
                        done(err);
                        return;
                    }
                    console.log(result);
                    done();
                });
            });
        });
    });
});
