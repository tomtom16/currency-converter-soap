module.exports = {
    mockXmlInput: "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
    "<gesmes:Envelope xmlns:gesmes=\"http://www.gesmes.org/xml/2002-08-01\" xmlns=\"http://www.ecb.int/vocabulary/2002-08-01/eurofxref\">\n" +
    "\t<gesmes:subject>Reference rates</gesmes:subject>\n" +
    "\t<gesmes:Sender>\n" +
    "\t\t<gesmes:name>European Central Bank</gesmes:name>\n" +
    "\t</gesmes:Sender>\n" +
    "\t<Cube>\n" +
    "\t\t<Cube time='2019-03-26'>\n" +
    "\t\t\t<Cube currency='USD' rate='1.1291'/>\n" +
    "\t\t\t<Cube currency='JPY' rate='124.72'/>\n" +
    "\t\t\t<Cube currency='BGN' rate='1.9558'/>\n" +
    "\t\t\t<Cube currency='CZK' rate='25.769'/>\n" +
    "\t\t\t<Cube currency='DKK' rate='7.4654'/>\n" +
    "\t\t\t<Cube currency='GBP' rate='0.85330'/>\n" +
    "\t\t\t<Cube currency='HUF' rate='316.10'/>\n" +
    "\t\t\t<Cube currency='PLN' rate='4.2934'/>\n" +
    "\t\t\t<Cube currency='RON' rate='4.7580'/>\n" +
    "\t\t\t<Cube currency='SEK' rate='10.4263'/>\n" +
    "\t\t\t<Cube currency='CHF' rate='1.1222'/>\n" +
    "\t\t\t<Cube currency='ISK' rate='136.90'/>\n" +
    "\t\t\t<Cube currency='NOK' rate='9.6398'/>\n" +
    "\t\t\t<Cube currency='HRK' rate='7.4188'/>\n" +
    "\t\t\t<Cube currency='RUB' rate='72.5964'/>\n" +
    "\t\t\t<Cube currency='TRY' rate='6.2404'/>\n" +
    "\t\t\t<Cube currency='AUD' rate='1.5832'/>\n" +
    "\t\t\t<Cube currency='BRL' rate='4.3568'/>\n" +
    "\t\t\t<Cube currency='CAD' rate='1.5132'/>\n" +
    "\t\t\t<Cube currency='CNY' rate='7.5818'/>\n" +
    "\t\t\t<Cube currency='HKD' rate='8.8622'/>\n" +
    "\t\t\t<Cube currency='IDR' rate='15999.35'/>\n" +
    "\t\t\t<Cube currency='ILS' rate='4.0892'/>\n" +
    "\t\t\t<Cube currency='INR' rate='77.7555'/>\n" +
    "\t\t\t<Cube currency='KRW' rate='1280.91'/>\n" +
    "\t\t\t<Cube currency='MXN' rate='21.5085'/>\n" +
    "\t\t\t<Cube currency='MYR' rate='4.5971'/>\n" +
    "\t\t\t<Cube currency='NZD' rate='1.6345'/>\n" +
    "\t\t\t<Cube currency='PHP' rate='59.404'/>\n" +
    "\t\t\t<Cube currency='SGD' rate='1.5254'/>\n" +
    "\t\t\t<Cube currency='THB' rate='35.730'/>\n" +
    "\t\t\t<Cube currency='ZAR' rate='16.2218'/>\n" +
    "\t\t</Cube>\n" +
    "\t</Cube>\n" +
    "</gesmes:Envelope>"
};