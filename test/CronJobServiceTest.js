const CronJobService = require('../services/CronJobService');
const ConversionService = require('../services/ConversionService');
const cache = require('../services/CacheService');
const mockXML = require('./mocks/MockXML');

describe('CronJobService', () => {
    let conversionService = new ConversionService();
    let cronJobService = new CronJobService();
    describe('basic', () => {
        it('should put stuff correctly into the cache', () => {
            let convertedMap = conversionService.convertAndExtract(mockXML.mockXmlInput);
            cronJobService.parseConvertedMapIntoCache(convertedMap, cache);
            console.log(cache.instance);
        });
    });
});