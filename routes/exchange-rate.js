const express = require('express');
const router = express.Router();
const url = 'https://www.ecb.europa.eu/stats/eurofxref/eurofxref-daily.xml';
const soap = require('soap');

/**
 * @deprecated
 * define routes
 */
router.get('/:base_currency/:new_currency/:value', function (req, res) {
    // http://localhost:3000/exchange-rate/USD/EUR/10
    res.send('Hello World!');
});

module.exports = router;