const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');

const indexRouter = require('./routes/index');
const exchangeRateRouter = require('./routes/exchange-rate');

const SoapServer = require('./controllers/SoapServer');
let soapServer = new SoapServer();

const app = express();

const CronJobService = require('./services/CronJobService');
let cronJob = new CronJobService();
cronJob.requestTask();
cronJob.registerJobs();

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/exchange-rate', exchangeRateRouter);
app.get('/wsdl/service-definition.wsdl', (req, res) => {
    res.contentType('application/xml');
    res.sendFile(path.join(__dirname , '/wsdl/service-definition.wsdl'));
});

app.server = app.listen('8000', () => {
    //Note: /exchange route will be handled by soap module
    //and all other routes & middleware will continue to work
    soapServer.bind('/ws', app);
});

module.exports = app;
