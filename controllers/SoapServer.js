const soap = require('soap');
const SoapService = require('../services/SoapService');
const fs = require('fs');

let soapService = new SoapService();

// DO NOT FORGET utf8 OR YOU WILL HAVE A VERY BAD TIME
let wsdl = fs.readFileSync('wsdl/service-definition.wsdl', 'utf8');

let SoapServer = function () {

    return {
        bind: function (path, webServer) {
            server = soap.listen(webServer, path, soapService, wsdl);

            server.authenticate = function (security, callback) {
                var token = security.UsernameToken;
                var user = token.Username;
                var password = token.Password;
                var nonce = token.Nonce;
                var created = token.Created;

                console.log(">>> ");
                console.log(user + ' ' + ' ' + created + ' ' + password['$value']);
                console.log('password digest: ' + soap.passwordDigest(password['$value'], created, 'secure-soap-password'));
                console.log('password digest: ' + soap.passwordDigest(nonce['$value'], created, 'secure-soap-password'));
                console.log('password digest: ' + soap.passwordDigest(password['$value'], created, nonce['$value']));
                console.log(">>> ");

                return user === "super-secure-user" &&
                    (
                        password['$value'] === soap.passwordDigest(nonce['$value'], created, 'secure-soap-password')
                        || password['$value'] === 'secure-soap-password'
                    );
            }
        }
    }

};

module.exports = SoapServer;
