const convert = require('xml-js');

let ConversionService = function () {
    return {
        convertAndExtract: function (xmlInput) {
            let convertedJSONString = convert.xml2json(xmlInput, { compact: true, spaces: 4 });
            let convertedJSON = JSON.parse(convertedJSONString);
            let resultingJSON = [];

            // Here we extract 'only the good part'
            convertedJSON = convertedJSON["gesmes:Envelope"]["Cube"]["Cube"]["Cube"];
            convertedJSON.forEach((element, index, collection) => {
                resultingJSON.push(element["_attributes"]);
            });
            return resultingJSON;
        }
    };
};

module.exports = ConversionService;
