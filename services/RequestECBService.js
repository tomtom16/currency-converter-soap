const request = require('request');

let RequestECBService = function () {

    let url = 'https://www.ecb.europa.eu/stats/eurofxref/eurofxref-daily.xml';
    let options = {};

    return {
        requestExchangeRates: function () {
            return new Promise((resolve, reject) => {
                request(url, options, (error, response, body) => {
                    if (error) {
                        console.error(error);
                        console.log(response);
                        reject(error);
                    }

                    console.debug("Received body => ");
                    console.debug(body);
                    resolve(body);
                });
            });
        }
    };
};

module.exports = RequestECBService;
