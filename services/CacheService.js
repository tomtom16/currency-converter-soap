module.exports = {
    instance: {},
    cacheCurrencyExchangeRate: function(currency, exchangeRate) {
        this.instance[currency] = exchangeRate;
    },
    getExchangeRateForCurrency: function(currency) {
        return this.instance[currency];
    },
    getCache: function() {
        if (this.instance.length === 0) {
            this.instance = createInstance();
        }
        return this.instance;
    }
};
