const cron = require('node-cron');
const RequestECBService = require('../services/RequestECBService');
const ConversionService = require('../services/ConversionService');
const cacheService = require('../services/CacheService');

let requestClient = new RequestECBService();
let conversionService = new ConversionService();

let CronJobService = function () {

    let alreadyCreated = false;
    let options = {};

    function isAlreadyCreated() {
        return alreadyCreated;
    }

    function hasBeenCreated() {
        alreadyCreated = true;
    }

    return {
        parseConvertedMapIntoCache: function (convertedMap, cache) {
            convertedMap.forEach((element, index, collection) => {
                if (element && element.currency && element.rate) {
                    cache.cacheCurrencyExchangeRate(element.currency, element.rate);
                } else {
                    console.error(element);
                    throw "Scheduled CronJob failed. ConvertedMap doesn't fit expected format: currency/rate";
                }
            });
        },
        requestTask: function () {
            let that = this;
            requestClient.requestExchangeRates()
                .then(result => {
                    // console.info("Scheduled CronJob succeeded. Message: ");
                    // console.info(result);

                    // [ { currency: 'XYZ', rate: '12.123123' } ]
                    let convertedMap = conversionService.convertAndExtract(result);
                    that.parseConvertedMapIntoCache(convertedMap, cacheService);
                })
                .catch(error => {
                    console.error("Scheduled CronJob didn't succeed. Error message:");
                    console.error(error);
                });
        },
        registerJobs: function () {
            if (! isAlreadyCreated()) {
                // seconds / minutes / hours / day of month / month / day of week
                cron.schedule('* 0 3 * * *', () => {
                    console.info("I just woke up. It is time to get to work.");
                    this.requestTask();
                }, options);
            }
            hasBeenCreated();
        }
    };
};

module.exports = CronJobService;