const ExchangeService = require('../services/ExchangeService');
let exchangeService = new ExchangeService();

let SoapService = function () {
    return {
        // Service
        ExchangePortService: {
            // Port
            ExchangePortSoap11: {
                // Operation
                exchangeCurrency: function (args) {
                    console.log('SoapService exchangeCurrency args:');
                    console.log(args);

                    if (args.request !== null) {
                        console.log('has request sub object...');
                        args = args.request;
                    }

                    let amount = exchangeService.convert(args.fromCurrency, args.toCurrency, args.fromCurrencyAmount);
                    console.log('amount:' + amount);
                    return {
                        response: {
                            toCurrency: args.toCurrency,
                            toCurrencyAmount: amount
                        }
                    };
                }
            }
        }
    }
};

module.exports = SoapService;
