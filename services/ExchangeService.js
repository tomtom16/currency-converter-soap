const cacheService = require('../services/CacheService');

let ExchangeService = function () {

    return {
        convert: function (fromCurrency, toCurrency, fromCurrencyAmount) {
            let toCurrencyAmount = NaN;

            if (! toCurrency || ! fromCurrencyAmount) {
                throw 'You have to pass in toCurrency and fromCurrencyAmount';
            }

            if (fromCurrency !== 'EUR') {
                let exchangeRateForCurrency = cacheService.getExchangeRateForCurrency(fromCurrency);
                fromCurrencyAmount = fromCurrencyAmount / exchangeRateForCurrency;
                toCurrencyAmount = fromCurrencyAmount;
            }

            if (toCurrency !== 'EUR') {
                let exchangeRateForCurrency = cacheService.getExchangeRateForCurrency(toCurrency);
                toCurrencyAmount = fromCurrencyAmount * exchangeRateForCurrency;
            }

            return toCurrencyAmount;
        }
    };

};

module.exports = ExchangeService;
